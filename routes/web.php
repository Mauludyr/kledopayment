<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
//payment
Route::get('/payment','App\Http\Controllers\PaymentController@index')->name('payment.index');
Route::get('/payment/add','App\Http\Controllers\PaymentController@add')->name('payment.add');
Route::post('/payment/save','App\Http\Controllers\PaymentController@save')->name('payment.save');
Route::get('/payment/edit/{id}','App\Http\Controllers\PaymentController@edit')->name('payment.edit');
Route::post('/payment/update/{id}','App\Http\Controllers\PaymentController@update')->name('payment.update');
Route::delete('/payment/destroy','App\Http\Controllers\PaymentController@destroy')->name('payment.destroy');