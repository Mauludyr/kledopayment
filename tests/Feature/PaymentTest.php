<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Queue;
use App\Jobs\DeletePaymentJob;
use App\Models\Payment;
use Tests\TestCase;
use Faker\Factory;
class PaymentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRequestAddPayment()
    {
        $faker = Factory::create();
        $payload = [
            'name' => $faker->name(),
        ];

        $this->json('POST', route('payment.save'), $payload)
            ->assertStatus(419);
    }

    public function testRequestDeletePayment()
    {
        $payments = Payment::select('id')->take(3)->get();
        foreach ($payments as $payment) {
            $payload['ids'][] = $payment;
        }

        $this->json('DELETE', route('payment.destroy'), $payload)
            ->assertStatus(419);
    }

    public function testRequestQueueSending()
    {
        Queue::fake();
        $payments = Payment::select('id')->take(3)->get();
        foreach ($payments as $payment) {
            DeletePaymentJob::dispatch($payment->id);
        }
        Queue::assertPushed(DeletePaymentJob::class);
    }
}
