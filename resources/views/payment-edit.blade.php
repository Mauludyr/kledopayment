@extends('app')
@section('content')
<h3>Tambah Data Payment</h3>
    <div class="row">
        <div class="col-12">
            <form action="{{ route('payment.update', $payment->id) }}" method="POST" id="formPayment" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label>Payment Name</label>
                            <input type="text" class="form-control" name="payment_name" id="payment_name" value="{{$payment->payment_name}}" required=""> 
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <button type="submit" name="update" id="update" class="btn btn-info btn-md">Update</button>
                            <a href="{{route('payment.index')}}" class="btn btn-primary btn-md"> Back</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
 
@endsection
@section('scripts')
@endsection