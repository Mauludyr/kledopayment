<!DOCTYPE html>
<html>

<head>
    <title>Payment Gate</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
    <div class="container-fluid">
    	@if ($message = Session::get('success'))
		  <div class="alert alert-success alert-block">
		    <button type="button" class="close" data-dismiss="alert">×</button> 
		      <strong>{{ $message }}</strong>
		  </div>
		@endif
        @yield('content')
    </div>
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    @yield('scripts')
    <script type="text/javascript">
    	$(document).ready(function() {   
		   $(".alert-success").fadeOut(5000);
		   $(".alert-warning").fadeOut(5000);
		});
    </script>
</body>

</html>