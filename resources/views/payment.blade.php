@extends('app')
@section('content')
<h3>Data Payment</h3>
<a href="{{ route('payment.add') }}" class="btn btn-primary btn-lg active mb-2" role="button" aria-pressed="true">Tambah Payment</a>
    <div class="row">
        <div class="col-12">
            <div class="alert alert-info alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong class="notification"></strong>
            </div>
            <a class="btn btn-danger mb-2" id="delete_payment" href="#">Delete Data</a>
            <div class="table-responsive">
                <table class="table table-striped table-hover" border="0" id="payment-table">
                    <thead>
                        <tr class="thead-dark">
                            <th><input type="checkbox" name="checkAll" id="checkAll"></th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $payment)
                        <tr>
                            <td width="5%"><input type="checkbox" class="select-checkbox" name="ids" id="ids" value="{{ $payment->id }}"></td>
                            <td width="70%">{{ $payment->payment_name }}</td>
                            <td width="20%">
                                <div class="form-group">
                                    <a class="btn btn-info" href="{{ route('payment.edit', $payment->id) }}">Edit</a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-6">
                <span class="d-block">    
                Halaman : {{ $data->currentPage() }} 
                </span> 
                <span class="d-block">
                Jumlah Data : {{ $data->total() }} 
                </span> 
                <span class="d-block">
                Data Per Halaman : {{ $data->perPage() }} 
                </span> 
        </div>
        <div class="col-6">
            {{ $data->links() }}
        </div>
    </div>
 
@endsection
@section('scripts')
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script type="text/javascript">
    $(function (e) {
        $("#checkAll").click(function(){
            $(".select-checkbox").prop('checked', $(this).prop('checked'));
        })

        $("#delete_payment").click(function(e){
            e.preventDefault();
            var ids = [];

            $("input:checkbox[name=ids]:checked").each(function(){
                ids.push($(this).val());
            });
            if (ids.length <= 0) {
                alert("Please select row.");
            } else {
             var check = confirm("Are you sure you want to delete this row?");
                if (check == true) {
                    $.ajax({
                        url:"{{route('payment.destroy')}}",
                        type:"DELETE",
                        data:{
                            _token:$('meta[name="csrf-token"]').attr('content'),
                            ids: ids,
                        },
                        success:function(data){
                            if (data['meta']) {
                            } else {
                                alert("Error");
                            }
                        },
                        error: function(data) {
                            alert(data.responseText);
                        }
                    })
                }
                $.each(ids, function(index, value) {
                    $('table tr').filter("[data-row-id='" + value + "']").remove();
                });
            }
        })
    })
</script>
<script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('b207384787bbde04c5eb', {
      cluster: 'ap1'
    });

    var channel = pusher.subscribe('my-channel');
    let count = 0;
    channel.bind('my-event', function(data) {
        count += 1;
         $(`.select-checkbox:checked[value="${data?.id}"]`).parents("tr").remove();
         $('.alert-info').css("display","block")
         $('.notification').text("Payment Deleted " + count + " Data")
         $(".alert-info").fadeOut(3000);
         setTimeout("location.reload(true);", 3000);
      // alert(JSON.stringify(data));
    });
    if (count == 0) {
        $('.alert-info').css("display","none")
    }
    $('.alert-info').css("display","none")
</script>
@endsection