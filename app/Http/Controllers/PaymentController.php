<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseCustom;
use Illuminate\Http\Request;
use App\Models\Payment;
use App\Jobs\DeletePaymentJob;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;

class PaymentController extends Controller
{
    protected $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->api_url = $this->url->to('/api');
    }

    public function index(Request $request)
    {
    	$data = Payment::orderBy('id', 'desc')->paginate(7);
    	return view('payment', compact('data'));
    }
    public function list(Request $request)
    {
    	$data = Payment::orderBy('id', 'desc')->paginate(7);
    	$response = array(
            "data" => $data
        );
        return response()->json($response);
    }

    public function add()
    {
    	return view('payment-add');
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payment_name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return back()->with(['errors' => 'Payment Name field is required']);
        }
        try {
            $payment = Payment::create([
                'payment_name' => $request->payment_name,
            ]);
            $payment->save();
            return redirect()->route('payment.index')->with(['success' => 'Payment Saved']);
        } catch (\Exception $e) {
            return back()->with(['errors' => 'Somenthing Errors']);
        }
    }

    public function edit($id)
    {
    	$payment = Payment::find($id);
    	return view('payment-edit', compact('payment'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'payment_name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return back()->with(['errors' => 'Payment Name field is required']);
        }
        $payment = Payment::find($id);
        if (!empty($payment)) {
            try {
                $payment->payment_name = $request->payment_name;
                $payment->save();
                return redirect()->route('payment.index')->with(['success' => 'Payment Updated']);
            } catch (\Exception $e) {
                return ResponseCustom::error($e->getMessage(), 400);
            }
        } else {
            return back()->with(['errors' => 'Somenthing Errors']);
        }
    }

    public function destroy(Request $request)
    {
    	try {
    		foreach ($request->ids as $id) {
	    		DeletePaymentJob::dispatch($id);
    		}
    		return ResponseCustom::success([], 'waiting to process');
    	} catch (\Exception $e) {
    		return ResponseCustom::error($e->getMessage(), 400);
    	}
    }

}
