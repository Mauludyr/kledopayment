<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->insert([
            ['id' => '1', 'payment_name' => 'Mandiri'],
            ['id' => '2', 'payment_name' => 'BCA'],
            ['id' => '3', 'payment_name' => 'BNI'],
            ['id' => '4', 'payment_name' => 'BRI'],
            ['id' => '5', 'payment_name' => 'Danamon'],
            ['id' => '6', 'payment_name' => 'BTN'],
            ['id' => '7', 'payment_name' => 'Syariah Indonesia'],
            ['id' => '8', 'payment_name' => 'BI'],
            ['id' => '9', 'payment_name' => 'Malaysian Bank'],
            ['id' => '10', 'payment_name' => 'OCBC'],
        ]);
    }
}
